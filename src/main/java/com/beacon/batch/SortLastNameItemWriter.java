package com.beacon.batch;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.core.io.FileSystemResource;

public class SortLastNameItemWriter implements ItemWriter<Player>, ItemStream {

	private FlatFileItemWriter<Player> delegate = new FlatFileItemWriter<Player>();
	private List<Player> playerList = new ArrayList();
	private Player previousPlayer = new Player();

	@Override
	public void write(List<? extends Player> players) throws Exception {

		for (Player player : players) {
			playerList.add(player);
		}

	}

	@Override
	public void close() {
		try {
			this.delegate.write(sortByLastName(playerList));
			this.delegate.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void open(ExecutionContext executionContext)
			throws ItemStreamException {

		DelimitedLineAggregator<Player> lineAggregator = new DelimitedLineAggregator<Player>();
		lineAggregator.setDelimiter(",");

		BeanWrapperFieldExtractor<Player> fieldExtractor = new BeanWrapperFieldExtractor<>();
		fieldExtractor.setNames(new String[] { "id", "firstName", "lastName",
				"position" });
		lineAggregator.setFieldExtractor(fieldExtractor);

		this.delegate
				.setResource(new FileSystemResource(
						"C:\\Users\\hazel.mae.t.cosina\\workspace-2\\beacon-batch\\result\\Players.txt"));
		this.delegate.setLineAggregator(lineAggregator);
		this.delegate.open(executionContext);
	}

	@Override
	public void update(ExecutionContext executionContext)
			throws ItemStreamException {
		this.delegate.update(executionContext);
	}

	private List<Player> sortByLastName(List<Player> players) {

		Collections.sort(playerList, new Comparator<Player>() {
			@Override
			public int compare(Player o1, Player o2) {
				if (o1.getLastName() == null) {
					previousPlayer = o2;
					return 0;
				} else {
					return o1.getLastName().compareTo(o2.getLastName());
				}
			}
		});
		return players;
	}

}
