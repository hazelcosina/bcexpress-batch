package com.beacon.batch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.core.io.FileSystemResource;

public class SortPositionItemWriter implements ItemWriter<Player>, ItemStream {

	private FlatFileItemWriter<Player> delegate = new FlatFileItemWriter<Player>();
	private List<Player> playerList = new ArrayList();
	private Player previousPlayer = new Player();
	private int positionOrder;

	@Override
	public void write(List<? extends Player> players) throws Exception {

		for (Player player : players) {
			playerList.add(player);
		}
	}

	@Override
	public void open(ExecutionContext executionContext)
			throws ItemStreamException {

		DelimitedLineAggregator<Player> lineAggregator = new DelimitedLineAggregator<Player>();
		lineAggregator.setDelimiter(",");

		BeanWrapperFieldExtractor<Player> fieldExtractor = new BeanWrapperFieldExtractor<>();
		fieldExtractor.setNames(new String[] { "id", "firstName", "lastName",
				"position" });
		lineAggregator.setFieldExtractor(fieldExtractor);

		this.delegate
				.setResource(new FileSystemResource(
						"C:\\Users\\hazel.mae.t.cosina\\workspace-2\\beacon-batch\\result\\Players2.txt"));
		this.delegate.setLineAggregator(lineAggregator);
		this.delegate.open(executionContext);

	}

	@Override
	public void update(ExecutionContext executionContext)
			throws ItemStreamException {
		this.delegate.update(executionContext);
	}

	@Override
	public void close() throws ItemStreamException {
		try {
			this.delegate.write(sortByPosition(playerList));
			this.delegate.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<Player> sortByPosition(List<Player> players) {

		Collections.sort(playerList, new Comparator<Player>() {

			@Override
			public int compare(Player o1, Player o2) {
				int player1PositionOrder = getPositionIndex(o1);
				int player2PositionOrder = getPositionIndex(o2);

				if (o1.getPosition() == null) {
					previousPlayer = o2;
					return 0;
				} else if (player1PositionOrder > player2PositionOrder) {
					return 1;
				} else if (player1PositionOrder < player2PositionOrder) {
					return -1;
				} else{
					return 0;
				}
			}
		});

		return players;
	}

	private int getPositionIndex(Player player) {
		int positionIndex = 0;
		String[] positions = new String[3];
		positions[0] = "Guard";
		positions[1] = "Forward";
		positions[2] = "Center";

		for (int x = 0; x < positions.length; x++) {
			String position = positions[x];

			if (position.equalsIgnoreCase(player.getPosition())) {
				positionIndex = x;
			}
		}

		return positionIndex;
	}
}
